<?php

namespace Drupal\acme_migrate\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\migrate_plus\Entity\Migration;

/**
 * Interactive configuration of the Acme migration process.
 */
class MigrationConfigurationForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'acme_migrate_migration_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['acme_blog_file'] = [
      '#type' => 'file',
      '#title' => $this->t('Blog data export file (CSV)'),
      '#description' => $this->t('Select an exported CSV file of blog data. Maximum file size is @size.',
        ['@size' => format_size(file_upload_max_size())]),
    ];

    $event_migration = Migration::load('event');
    $source = $event_migration->get('source');
    if (!empty($source['urls'])) {
      if (is_array($source['urls'])) {
        $default_value = reset($source['urls']);
      }
      else {
        $default_value = $source['urls'];
      }
    }
    else {
      $default_value = 'http://services.example.com/CFService.asmx?wsdl';
    }
    $form['acme_event'] = [
      '#type' => 'details',
      '#title' => $this->t('Event migration'),
      '#open' => TRUE,
    ];
    $form['acme_event']['event_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CF service endpoint for retrieving event data'),
      '#default_value' => $default_value,
    ];
    $form['acme_event']['event_clientid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID for the CF service'),
      '#default_value' => @$source['parameters']['clientId'] ?: 1234,
    ];
    $form['acme_event']['event_password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password for the CF service'),
      '#default_value' => @$source['parameters']['clientCredential']['Password'] ?: '',
    ];

    $doctor_migration = Migration::load('doctor');
    $source = $doctor_migration->get('source');
    if (!empty($source['urls'])) {
      if (is_array($source['urls'])) {
        $default_value = reset($source['urls']);
      }
      else {
        $default_value = $source['urls'];
      }
    }
    else {
      $default_value = 'https://kservice.example2.com/providers';
    }
    $form['acme_doctor'] = [
      '#type' => 'details',
      '#title' => $this->t('Doctor migration'),
      '#open' => TRUE,
    ];
    $form['acme_doctor']['doctor_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('K service endpoint for retrieving doctor data'),
      '#maxlength' => 1024,
      '#default_value' => $default_value,
    ];
    $form['acme_doctor']['doctor_base_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base URI for retrieving access tokens'),
      '#default_value' => @$source['authentication']['base_uri'] ?: 'https://kservice.example2.com',
    ];
    $form['acme_doctor']['doctor_client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID for the K service'),
      '#default_value' => @$source['authentication']['client_id'] ?: 'default_client_id',
    ];
    $form['acme_doctor']['doctor_client_secret'] = [
      '#type' => 'password',
      '#title' => $this->t('Client secret for the K service'),
      '#default_value' => @$source['authentication']['client_secret'] ?: '',
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $all_files = $this->getRequest()->files->get('files', []);
    if (!empty($all_files['acme_blog_file'])) {
      $validators = ['file_validate_extensions' => ['csv']];
      if ($file = file_save_upload('acme_blog_file', $validators, 'public://', 0)) {
        $blog_migration = Migration::load('blog');
        $source = $blog_migration->get('source');
        $source['path'] = $file->getFileUri();
        $blog_migration->set('source', $source);
        $blog_migration->save();
        drupal_set_message($this->t('File uploaded as @uri.', ['@uri' => $file->getFileUri()]));
      }
      else {
        drupal_set_message($this->t('File upload failed.'));
      }
    }

    $event_migration = Migration::load('event');
    $source = $event_migration->get('source');
    $source['urls'] = $form_state->getValue('event_endpoint');
    $source['parameters'] = [
      'clientId' => $form_state->getValue('event_clientid'),
      'clientCredential' => [
        'ClientID' => $form_state->getValue('event_clientid'),
        'Password' => $form_state->getValue('event_password'),
      ],
      'startDate' => date('m-d-Y'),
    ];
    $event_migration->set('source', $source);
    $event_migration->save();
    drupal_set_message($this->t('Event migration configuration saved.'));

    $doctor_migration = Migration::load('doctor');
    $source = $doctor_migration->get('source');
    $source['urls'] = $form_state->getValue('doctor_endpoint');
    $source['authentication'] = array_merge($source['authentication'], [
      'base_uri' => $form_state->getValue('doctor_base_uri'),
      'client_id' => $form_state->getValue('doctor_client_id'),
      'client_secret' => $form_state->getValue('doctor_client_secret'),
    ]);
    $doctor_migration->set('source', $source);
    $doctor_migration->save();
    drupal_set_message($this->t('Doctor migration configuration saved.'));
  }

}
