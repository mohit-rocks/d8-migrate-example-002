<?php

namespace Drupal\acme_migrate\EventSubscriber;

use Drupal\file\Entity\File;
use Drupal\migrate\Row;
use Drupal\migrate_plus\Event\MigrateEvents;
use Drupal\migrate_plus\Event\MigratePrepareRowEvent;
use Drupal\node\Entity\Node;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Cleanup of raw data.
 */
class PrepareRow implements EventSubscriberInterface {

  /**
   * IDs of any file entities created while parsing the body text.
   *
   * @var array
   */
  protected $imageFids = [];

  /**
   * Cache of all class objects from the feed.
   *
   * @var object[]
   */
  protected $classes = [];

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[MigrateEvents::PREPARE_ROW] = 'onMigratePrepareRow';
    return $events;
  }

  /**
   * Cleanup some dirty data.
   *
   * @param \Drupal\migrate_plus\Event\MigratePrepareRowEvent $event
   *   The prepareRow event.
   */
  public function onMigratePrepareRow(MigratePrepareRowEvent $event) {
    $row = $event->getRow();
    switch ($event->getMigration()->id()) {
      case 'doctor':
        // Massage the updated time into a proper timestamp.
        $last_updated = $row->getSourceProperty('last_updated');
        $last_updated = str_replace('T', ' ', $last_updated);
        $last_updated = substr($last_updated, 0, 19);
        $row->setSourceProperty('last_updated', strtotime($last_updated));
        // Construct the K URL.
        $encoded_name = urlencode($row->getSourceProperty('full_name'));
        $k_url = 'https://kservice.example2.com/' . $encoded_name .
          '/' . $row->getSourceProperty('id');
        $row->setSourceProperty('k_url', $k_url);
        // Extract specialty and subspecialty terms.
        $specialty = [];
        $subspecialty = [];
        foreach ($row->getSourceProperty('specialties') as $specialty_info) {
          if ($specialty_info['specialty']) {
            $specialty[$specialty_info['specialty']] = $specialty_info['specialty'];
          }
          if ($specialty_info['subspecialty']) {
            $subspecialty[$specialty_info['subspecialty']] = $specialty_info['subspecialty'];
          }
        }
        $row->setSourceProperty('specialty', $specialty);
        $row->setSourceProperty('subspecialty', $subspecialty);
        break;

      case 'location':
        $this->cleanData($row);
        // One known bad state.
        if ($row->getSourceProperty('state') == 'IIL') {
          $row->setSourceProperty('state', 'IL');
        }
        // Break street address into two parts.
        $street_address = $row->getSourceProperty('streetAddress');
        if (is_array($street_address)) {
          $street_address = reset($street_address);
        }
        if ($street_address) {
          $address_lines = explode(',', $street_address);
          $row->setSourceProperty('streetAddress1', trim($address_lines[0]));
          if (!empty($address_lines[1])) {
            $row->setSourceProperty('streetAddress2', trim($address_lines[1]));
          }
        }
        break;

      case 'blog':
        $this->cleanData($row);
        $strip_strings = [
          '##continueReading',
          '##fullsiteonly##',
          '##ENDfullsiteonly##',
        ];
        $row->setSourceProperty('content', str_replace($strip_strings, '',
          $row->getSourceProperty('content')));

        // Construct the metatags field value.
        $description = $row->getSourceProperty('metaDescription');
        $keywords = $row->getSourceProperty('metaKeywords');
        $metatags = [
          'description' => $description,
          'keywords' => $keywords,
        ];
        $row->setSourceProperty('metatags', serialize($metatags));

        // Find or create a person node corresponding to the author.
        $author = $row->getSourceProperty('author');
        $words = explode(' ', $author);
        $first_name = $words[0];
        unset($words[0]);
        if (empty($words)) {
          $last_name = ' ';
        }
        else {
          $last_name = implode(' ', $words);
        }
        $person_nids = \Drupal::entityQuery('node')
          ->condition('type', 'person')
          ->condition('field_first_name', $first_name)
          ->condition('field_last_name', $last_name)
          ->execute();
        if (!empty($person_nids)) {
          $row->setSourceProperty('author', reset($person_nids));
        }
        else {
          $person_values = [
            'type' => 'person',
            'field_first_name' => $first_name,
            'field_last_name' => $last_name,
            'uid' => 1,
          ];
          $person_node = Node::create($person_values);
          $person_node->save();
          $row->setSourceProperty('author', $person_node->id());
        }

        // Handle embedded local image references (those beginning with / or
        // http://acme.com).
        $this->imageFids = [];
        $pattern = '!src=[\'"](?:/|http://(?:www.)acme.com/)(.*?)[\'"]!i';
        $content = preg_replace_callback($pattern, [$this, 'replaceImages'],
          $row->getSourceProperty('content'));

        // If the first "real" element in the body is an img tag, take the first
        // migrated image as field_main_image, and strip the reference from the
        // body.
        if (!empty($this->imageFids)) {
          $replaced_content = preg_replace('|^(<p[^<]*>)<img.*?>|i', '$1', $content);
          if ($content != $replaced_content) {
            $row->setSourceProperty('main_image', reset($this->imageFids));
            $content = $replaced_content;
          }
        }
        $row->setSourceProperty('content', $content);
        break;

      case 'service':
        $this->cleanData($row);
        break;

      case 'event':
        // Cache the class list, indexed by ClassID.
        if (empty($this->classes)) {
          $source = $row->getSource();
          $client = new \SoapClient(reset($source['urls']));
          $base_parameters = [
            'clientCredential' => $source['parameters']['clientCredential'],
            'clientID' => $source['parameters']['clientId'],
          ];
          $class_parameters = $base_parameters + ['serviceLineID' => ''];
          $response = $client->GetClientClassesByServiceLineId($class_parameters);
          foreach ($response->GetClientClassesByServiceLineIdResult->ClientClassBOL as $class) {
            $this->classes[$class->ClassID] = $class;
          }
          // GetClientClassesByServiceLineId does not include service line
          // references. The only way to derive these references is to make this
          // query for each service line individually. So, first we get the list
          // of service lines.
          $response = $client->GetClientServiceLines($base_parameters);
          foreach ($response->GetClientServiceLinesResult->ClientServiceLine as $service_line) {
            $class_parameters['serviceLineID'] = $service_line->ServiceLineID;
            $response = $client->GetClientClassesByServiceLineId($class_parameters);
            if (isset($response->GetClientClassesByServiceLineIdResult->ClientClassBOL)) {
              foreach ($response->GetClientClassesByServiceLineIdResult->ClientClassBOL as $class) {
                if (isset($this->classes[$class->ClassID])) {
                  $this->classes[$class->ClassID]->ServiceLineName = $service_line->CustomName;
                }
              }
            }
          }
        }
        $class = $this->classes[$row->getSourceProperty('ClassID')];
        $row->setSourceProperty('ClassName', $class->ClassName);
        $row->setSourceProperty('ClassDescription', $class->ClassDescription);
        $row->setSourceProperty('ServiceLineName', $class->ServiceLineName);
        if (strlen($class->ClassDescription) > 600) {
          $short_description = substr($class->ClassDescription, 0, 597) . '...';
        }
        else {
          $short_description = $class->ClassDescription;
        }
        $row->setSourceProperty('ShortClassDescription', $short_description);
        $row->setSourceProperty('RemainingSeats', $row->getSourceProperty('TotalSeats') - $row->getSourceProperty('FilledSeats'));
        $session_location = $row->getSourceProperty('SessionLocation');
        $row->setSourceProperty('AddressLineOne', $session_location->AddressLineOne);
        $row->setSourceProperty('AddressLineTwo', $session_location->AddressLineTwo);
        $row->setSourceProperty('City', $session_location->City);
        $row->setSourceProperty('State', $session_location->State);
        $zip_pieces = array_filter([
          $session_location->ZipCode->FirstPart,
          $session_location->ZipCode->SecondPart,
        ]);
        $row->setSourceProperty('ZipCode', implode('-', $zip_pieces));
        $row->setSourceProperty('BusinessName', $session_location->BusinessName);
        $row->setSourceProperty('StartDateTime', $this->adjustDate(
          $row->getSourceProperty('StartDateTime')
        ));
        $row->setSourceProperty('EndDateTime', $this->adjustDate(
          $row->getSourceProperty('EndDateTime')
        ));
        break;

      default:
        break;
    }
  }

  /**
   * Adjust a date in the local timezone to UTC.
   *
   * @param string $local_date
   *   Local datetime string in the form YYYY-MM-DDTHH:MM:SS.
   *
   * @return string
   *   UTC datetime string in the form YYYY-MM-DDTHH:MM:SS.
   */
  protected function adjustDate($local_date) {
    $adjusted_date = gmdate('c', strtotime($local_date));
    return strtok($adjusted_date, '+');
  }

  /**
   * Replace local image references to point to the appropriate Drupal paths.
   *
   * @param array $matches
   *   Matched text from preg_replace_callback.
   */
  protected function replaceImages(array $matches) {
    // The src attribute value (minus a leading / or http://acme.com/).
    $src = $matches[1];
    $source_path = 'http://acme.com/' . $src;
    $drupal_path = 'public://' . $src;
    // If it has already been migrated, get its fid.
    $fids = \Drupal::entityQuery('file')
      ->condition('uri', $drupal_path)
      ->execute();
    if (empty($fids)) {
      // Copy the file in.
      if (file_prepare_directory(dirname($drupal_path), FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
        $result = @copy($source_path, $drupal_path);
        if ($result) {
          \Drupal::service('file_system')->chmod($drupal_path);
          // Create the file entity.
          $file_values = [
            'uri' => $drupal_path,
            'uid' => 1,
            'status' => 1,
          ];
          $file = File::create($file_values);
          $file->save();
        }
        else {
          // File copy failed, do not change the reference.
          return $matches[0];
        }
      }
      else {
        // Prepare directory failed, do not change the reference.
        return $matches[0];
      }
    }
    else {
      $file = File::load(reset($fids));
    }
    $this->imageFids[] = $file->id();
    return "src='" . $file->url() . "'";
  }

  /**
   * Cleanup to apply to every source property.
   *
   * @param \Drupal\migrate\Row $row
   *   The row to clean.
   */
  protected function cleanData(Row $row) {
    foreach ($row->getSource() as $key => $value) {
      // Source data is not clean UTF-8.
      if (is_array($value)) {
        $row->setSourceProperty($key, array_map('utf8_encode', $value));
      }
      elseif ($value) {
        $row->setSourceProperty($key, utf8_encode($value));
      }
      // Replace string 'NULL' with a proper PHP NULL.
      if (!is_array($value) && strcasecmp($value, 'NULL') == 0) {
        $row->setSourceProperty($key, NULL);
      }
    }
  }

}
