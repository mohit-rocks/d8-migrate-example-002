<?php

namespace Drupal\acme_migrate\EventSubscriber;

use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Event\MigrateImportEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Handle disabling automatic geocoding during location migration.
 */
class GeoFieldConfigurator implements EventSubscriberInterface {

  /**
   * The geocoder settings before we changed them.
   *
   * @var array
   */
  protected $originalSettings;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[MigrateEvents::PRE_IMPORT] = 'onMigrationPreImport';
    $events[MigrateEvents::POST_IMPORT] = 'onMigrationPostImport';
    return $events;
  }

  /**
   * Disable automatic geocoding for field_geofield.
   *
   * @param \Drupal\migrate\Event\MigrateImportEvent $event
   *   The import event.
   */
  public function onMigrationPreImport(MigrateImportEvent $event) {
    if ($event->getMigration()->id() == 'location') {
      $fields = \Drupal::entityTypeManager()->getStorage('field_config')->loadByProperties(['field_name' => 'field_geofield']);
      if ($fields) {
        /** @var \Drupal\field\Entity\FieldConfig $field */
        if ($field = $fields['node.location.field_geofield']) {
          $this->originalSettings = $field->getThirdPartySettings('geocoder_field');
          $field->setThirdPartySetting('geocoder_field', 'method', 'none');
          $field->save();
        }
      }
    }
  }

  /**
   * Re-enable automatic geocoding for field_geofield.
   *
   * @param \Drupal\migrate\Event\MigrateImportEvent $event
   *   The import event.
   */
  public function onMigrationPostImport(MigrateImportEvent $event) {
    if ($event->getMigration()->id() == 'location') {
      $fields = \Drupal::entityTypeManager()->getStorage('field_config')->loadByProperties(['field_name' => 'field_geofield']);
      if ($fields) {
        /** @var \Drupal\field\Entity\FieldConfig $field */
        if ($field = $fields['node.location.field_geofield']) {
          foreach ($this->originalSettings as $key => $value) {
            $field->setThirdPartySetting('geocoder_field', $key, $value);
          }
          $field->save();
        }
      }
    }
  }

}
