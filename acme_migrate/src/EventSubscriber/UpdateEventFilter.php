<?php

namespace Drupal\acme_migrate\EventSubscriber;

use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Event\MigrateImportEvent;
use Drupal\migrate_plus\Entity\Migration;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Make sure the start date filter for event migration is up-to-date.
 */
class UpdateEventFilter implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[MigrateEvents::PRE_IMPORT] = 'onMigrationPreImport';
    return $events;
  }

  /**
   * Set the event start date filter to today.
   *
   * @param \Drupal\migrate\Event\MigrateImportEvent $event
   *   The import event.
   */
  public function onMigrationPreImport(MigrateImportEvent $event) {
    if ($event->getMigration()->id() == 'event') {
      $event_migration = Migration::load('event');
      $source = $event_migration->get('source');
      $source['parameters']['startDate'] = date('m-d-Y');
      $event_migration->set('source', $source);
      $event_migration->save();
    }
  }

}
