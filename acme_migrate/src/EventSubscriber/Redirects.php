<?php

namespace Drupal\acme_migrate\EventSubscriber;

use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Event\MigratePostRowSaveEvent;
use Drupal\redirect\Entity\Redirect;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Add redirects.
 */
class Redirects implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[MigrateEvents::POST_ROW_SAVE] = 'onMigrationPostRowSave';
    return $events;
  }

  /**
   * Add redirects for location nodes.
   *
   * @param \Drupal\migrate\Event\MigratePostRowSaveEvent $event
   *   The post-save event.
   */
  public function onMigrationPostRowSave(MigratePostRowSaveEvent $event) {
    $redirects = [];
    switch ($event->getMigration()->id()) {
      case 'location':
        $source_id = $event->getRow()->getSourceProperty('itemValue');
        $destination_id = $event->getDestinationIdValues();
        $redirect_properties = [
          'redirect_redirect' => 'entity:node/' . reset($destination_id),
          'status_code' => 301,
          'uid' => $event->getRow()->getDestinationProperty('uid'),
        ];
        // Handle the legacy detail link.
        $redirects[] = $redirect_properties + [
          'redirect_source' => [
            'path' => 'locations/locationDetail.aspx',
            'query' => ['id' => $source_id],
          ],
        ];
        // If there's a local or acme.com website link, redirect it.
        $urls = array_filter([
          $event->getRow()->getSourceProperty('Website'),
          $event->getRow()->getSourceProperty('alternateUrl'),
        ]);
        foreach ($urls as $url) {
          $url = preg_replace('|http://(www.)?acme.com/|', '', $url);
          // Ignore other websites.
          if (substr($url, 0, 7) != 'http://') {
            $redirects[] = $redirect_properties + [
              'redirect_source' => [
                'path' => $url,
                'query' => [],
              ],
            ];
          }
        }
        break;

      case 'service':
        if ($overwrite_url = $event->getRow()->getSourceProperty('overwriteURL')) {
          $overwrite_url = str_replace('http://www.acme.com/', '', $overwrite_url);
          $destination_id = $event->getDestinationIdValues();
          $redirects[] = [
            'redirect_source' => [
              'path' => ltrim($overwrite_url, '/'),
              'query' => [],
            ],
            'redirect_redirect' => 'entity:node/' . reset($destination_id),
            'status_code' => 301,
            'uid' => $event->getRow()->getDestinationProperty('uid'),
          ];
        }
        break;
    }
    foreach ($redirects as $redirect) {
      // See if we already have this redirect.
      $hash = Redirect::generateHash($redirect['redirect_source']['path'],
        $redirect['redirect_source']['query'], 'en');
      $redirects = \Drupal::entityManager()
        ->getStorage('redirect')
        ->loadByProperties(['hash' => $hash]);
      if (empty($redirects)) {
        $redirect_entity = Redirect::create($redirect);
        $redirect_entity->save();
      }
    }
  }

}
