Migration from various content sources to a new Drupal 8 instance.

To configure the migrations:

1. Go to /admin/structure/migrate/acme_migrate.
2. Upload the blog CSV file.
3. Enter the endpoint and credentials for the CF service.
4. Enter the endpoint and credentials for the K service.

To manage the migration process:

# migrate-status command
docroot$ drush ms
 Group: acme  Status  Total  Imported  Unprocessed  Last imported
 blog        Idle    159    0         159
 doctor      Idle    2313   0         2313
 event       Idle    250    0         250
 location    Idle    148    0         148
 service     Idle    27     0         27

# migrate-import command - run all migrations
docroot$ drush mi --all
Processed 159 items (159 created, 0 updated, 0 failed, 0 ignored) - done with 'blog'
Processed 148 items (148 created, 0 updated, 0 failed, 0 ignored) - done with 'location'
...

# Run one migration
docroot$ drush mi location
Processed 148 items (148 created, 0 updated, 0 failed, 0 ignored) - done with 'location'

# Run a small sample for testing
docroot$ drush mi location --limit=10
Processed 10 items (10 created, 0 updated, 0 failed, 0 ignored) - done with 'location'

# Migrate a single item for testing (with source ID 4)
docroot$ drush mi location --idlist=4
Processed 1 item (1 created, 0 updated, 0 failed, 0 ignored) - done with 'location'

# migrate-rollback command
drush mr --all
Rolled back 148 items - done with 'location'
...

drush mr location
Rolled back 148 items - done with 'location'

# Check for error messages
docroot$ drush mmsg location
No messages for this migration
