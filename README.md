# d8-migrate-example-002

This module, implemented on behalf of [Acquia](https://www.acquia.com/) for their (fictionalized) client Acme, demonstrates the following Drupal 8 migration techniques:

1. Configuring migration though the admin UI.
2. Migration from CSV files.
3. Migration from a JSON feed.
4. Migration from a SOAP feed.
5. Migration from an OAuth-authenticated feed.

See http://virtuoso-performance.com/blog/mikeryan/drupal-8-migration-soap-api for more information.
